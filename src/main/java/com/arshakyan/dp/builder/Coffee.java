package com.arshakyan.dp.builder;

/**
 * Created by ezalor on 08.02.2015.
 */
public class Coffee {
    private String coffeeType;
    private String producerCountry;
    private Boolean withMilk;


    public void setCoffeeType(String coffeeType) {
        this.coffeeType = coffeeType;
    }

    public void setProducerCountry(String producerCountry) {
        this.producerCountry = producerCountry;
    }

    public void setWithMilk(Boolean withMilk) {
        this.withMilk = withMilk;
    }
}
