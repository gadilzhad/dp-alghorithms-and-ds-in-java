package com.arshakyan.dp.builder;

/**
 * Created by ezalor on 08.02.2015.
 */
abstract class CoffeeBuilder {
    protected Coffee coffee;

    public Coffee getCoffee(){
        return coffee;
    }

    public void createSomeCoffee(){
        coffee = new Coffee();
    }

    public abstract void buildCoffeeType();
    public abstract void buildWithMilk();
    public abstract void buildProducerCountry();
}
