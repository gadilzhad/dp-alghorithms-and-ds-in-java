package com.arshakyan.dp.builder;

/**
 * Created by ezalor on 08.02.2015.
 */
public class CoffeeMachine {
    private CoffeeBuilder coffeeBuilder;


    public void setCoffeeBuilder(CoffeeBuilder coffeeBuilder) {
        this.coffeeBuilder = coffeeBuilder;
    }

    public Coffee getCoffee(){
        return coffeeBuilder.getCoffee();
    }

    public void constructCoffee(){
        coffeeBuilder.createSomeCoffee();
        coffeeBuilder.buildCoffeeType();
        coffeeBuilder.buildProducerCountry();
        coffeeBuilder.buildWithMilk();
    }

}
