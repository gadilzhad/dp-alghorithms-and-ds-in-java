package com.arshakyan.dp.builder;

/**
 * Created by ezalor on 08.02.2015.
 */
public class LatteCoffeeBuilder extends CoffeeBuilder {
    @Override
    public void buildCoffeeType() {
        coffee.setCoffeeType("latte");
    }

    @Override
    public void buildWithMilk() {
        coffee.setWithMilk(true);
    }

    @Override
    public void buildProducerCountry() {
        coffee.setProducerCountry("Brazil");
    }
}
