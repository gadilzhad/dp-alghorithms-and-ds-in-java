package com.arshakyan.dp.builder;

/**
 * Created by ezalor on 08.02.2015.
 */
public class EspressoCoffeeBuilder extends CoffeeBuilder {
    @Override
    public void buildCoffeeType() {
        coffee.setCoffeeType("espresso");
    }

    @Override
    public void buildWithMilk() {
        coffee.setWithMilk(false);
    }

    @Override
    public void buildProducerCountry() {
        coffee.setProducerCountry("Peru");
    }
}
