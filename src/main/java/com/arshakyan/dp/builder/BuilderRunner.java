package com.arshakyan.dp.builder;

/**
 * Created by ezalor on 08.02.2015.
 */
public class BuilderRunner {
    public static void main(String[] args){
        CoffeeMachine coffeeMachine = new CoffeeMachine();
        CoffeeBuilder coffeeBuilder = new EspressoCoffeeBuilder();
        coffeeMachine.setCoffeeBuilder(coffeeBuilder);
        coffeeMachine.constructCoffee();

        Coffee coffee  = coffeeMachine.getCoffee();
    }

}
