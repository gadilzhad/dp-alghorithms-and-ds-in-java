package com.arshakyan.dp.strategy;

/**
 * Created by ezalor on 03.02.2015.
 */
public class PokerPlayer {
    private PokerStrategy strategy;


    PokerPlayer(PokerStrategy strategy){
        this.strategy = strategy;
    }

    public void changeStrategy(PokerStrategy strategy){
        this.strategy = strategy;
    }

    public void playSomePoker(){}

}
