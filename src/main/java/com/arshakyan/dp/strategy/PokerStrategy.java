package com.arshakyan.dp.strategy;

/**
 * Created by ezalor on 03.02.2015.
 */
public interface PokerStrategy {
    public void execute();
}
