package com.arshakyan.dp.observer;

/**
 * Created by ezalor on 08.02.2015.
 */
public interface Observer {
    public void update(int orders, int freeTables);
}
