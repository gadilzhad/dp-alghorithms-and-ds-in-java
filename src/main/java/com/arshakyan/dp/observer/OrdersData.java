package com.arshakyan.dp.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ezalor on 08.02.2015.
 */
public class OrdersData implements Observable {

    private List<Observer> observerList;
    private int orders;
    private int freeTables;

    OrdersData()
    {
        observerList = new ArrayList<Observer>();
    }

    @Override
    public void registerObserver(Observer o) {
        observerList.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observerList.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observerList)
        {
            observer.update(orders, freeTables);
        }
    }

    public void setMeasurements(int orders, int freeTables)
    {
        this.orders = orders;
        this.freeTables = freeTables;
        notifyObservers();
    };

}
