package com.arshakyan.dp.observer;

/**
 * Created by ezalor on 08.02.2015.
 */
public class CurrentStateDisplay implements Observer {
    private int orders;
    private int freeTables;
    private OrdersData ordersData;


    CurrentStateDisplay(OrdersData ordersData)
    {
        this.ordersData = ordersData;
        ordersData.registerObserver(this);
    }

    @Override
    public void update(int orders, int freeTables) {
        this.orders = orders;
        this.freeTables = freeTables;
        this.display();
    }

    private void display() {
        System.out.println("Orders: " + orders + "  |  Free Tables : " + freeTables);
    }
}
