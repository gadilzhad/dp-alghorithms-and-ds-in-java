package com.arshakyan.dp.observer;

/**
 * Created by ezalor on 08.02.2015.
 */
public interface Observable {
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers();
}
