package com.arshakyan.dp.observer;

/**
 * Created by ezalor on 08.02.2015.
 */
public class Restaurant {
    public static void main(String[] args){
        OrdersData ordersData = new OrdersData();

        CurrentStateDisplay currentDisplay = new CurrentStateDisplay(ordersData);
        ordersData.setMeasurements(5, 12);
        ordersData.setMeasurements(8, 9);
        ordersData.setMeasurements(15,4);
    }
}
