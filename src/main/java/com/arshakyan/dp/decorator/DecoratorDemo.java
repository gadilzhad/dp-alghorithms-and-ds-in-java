package com.arshakyan.dp.decorator;

/**
 * Created by ezalor on 11.02.2015.
 */
public class DecoratorDemo {

    public static void main(String[] args){
        Vehicle car = new Car();
        Vehicle bike = new Bike();
        Vehicle blackCar = new BlackVehicleDecorator(new Car());


        car.build();
        bike.build();
        blackCar.build();
    }

}
