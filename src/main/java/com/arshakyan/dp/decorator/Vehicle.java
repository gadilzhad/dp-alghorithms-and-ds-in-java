package com.arshakyan.dp.decorator;

/**
 * Created by ezalor on 11.02.2015.
 */
public interface Vehicle {
    public void build();
}
