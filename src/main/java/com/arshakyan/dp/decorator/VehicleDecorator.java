package com.arshakyan.dp.decorator;

/**
 * Created by ezalor on 11.02.2015.
 */
public abstract class VehicleDecorator implements Vehicle {
       protected Vehicle decoratedVehicle;

        public VehicleDecorator(Vehicle decoratedVehicle){
            this.decoratedVehicle = decoratedVehicle;
        }

        public void build(){
            decoratedVehicle.build();
        }

}

