package com.arshakyan.dp.decorator;

/**
 * Created by ezalor on 11.02.2015.
 */
public class BlackVehicleDecorator extends VehicleDecorator {
    public BlackVehicleDecorator(Vehicle decoratedVehicle) {
        super(decoratedVehicle);
    }

    @Override
    public void build() {
        decoratedVehicle.build();
        setBlackColor(decoratedVehicle);
    }

    private void setBlackColor(Vehicle decoratedVehicle){
        System.out.println("Vehicle color: Black");
    }
}
