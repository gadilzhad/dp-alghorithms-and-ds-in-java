package com.arshakyan.dp.decorator;

/**
 * Created by ezalor on 11.02.2015.
 */
public class Bike implements Vehicle {
    @Override
    public void build() {
        System.out.println("Vehicle: Bike");
    }
}
