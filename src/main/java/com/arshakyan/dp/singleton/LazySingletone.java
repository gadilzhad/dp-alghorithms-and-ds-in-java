package com.arshakyan.dp.singleton;

/**
 * Created by ezalor on 02.02.2015.
 */

/**
 * more lazy realization with serialization and threadsafety out of the box
 */


 public enum LazySingletone {
    INSTANCE;
}
