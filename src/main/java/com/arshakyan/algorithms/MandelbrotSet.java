package com.arshakyan.algorithms;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by ezalor on 12.02.2015.
 */
public class MandelbrotSet extends JFrame {
    private final int MAX_ITERATIONS = 70;
    private final double ZOOM = 250;
    private BufferedImage mandelbrotImage;
    private double zX, zY, cX, cY;

    public MandelbrotSet() {
        super("Mandelbrot Set");
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        createMandelbrotSet();
    }

    private void createMandelbrotSet() {
        double tmp;
        mandelbrotImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                zX = zY = 0;
                cX = (x - 400) / ZOOM;
                cY = (y - 300) / ZOOM;
                int counter = MAX_ITERATIONS;
                while (zX * zX + zY * zY < 4 && counter > 0) {
                    tmp = zX * zX - zY * zY + cX;
                    zY = 2.0 * zX * zY + cY;
                    zX = tmp;
                    counter--;
                }
                mandelbrotImage.setRGB(x, y, counter | (counter << 8));
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(mandelbrotImage, 0, 0, this);
    }

    public static void main(String[] args) {
        new MandelbrotSet().setVisible(true);
    }
}
