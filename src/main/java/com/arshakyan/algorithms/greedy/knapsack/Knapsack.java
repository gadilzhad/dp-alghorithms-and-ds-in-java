package com.arshakyan.algorithms.greedy.knapsack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ezalor on 10.02.2015.
 */
public class Knapsack {
    private int numOfElements;
    private List<Double> weights;
    private List<Double> profits;
    private List<Double> ratio;
    private double capacity;


    public Knapsack(int numOfElements, List<Double> weights, List<Double> profits, double capacity) {
        this.numOfElements = numOfElements;
        this.weights = weights;
        this.profits = profits;
        this.capacity = capacity;
        ratio = new ArrayList<Double>();
    }

    public void resolve(){
        calculateRatio();
        sortByRatio();
        List<Double> resultVector = new ArrayList<Double>();
        double maximumProfit = 0;
        double tempCapacity = capacity;
        int i;

        resultVector = initializeWithZeroes(resultVector);

        for (i = 0; i < numOfElements; i++) {
            if (weights.get(i) > tempCapacity)
                break;
            else {
                resultVector.set(i, resultVector.get(i)+1.0);
                maximumProfit += profits.get(i);
                tempCapacity -= weights.get(i);
            }

        }

        if (i < numOfElements)
            resultVector.set(i, (tempCapacity / weights.get(i)));

        maximumProfit += resultVector.get(i)*profits.get(i);

        System.out.print(resultVector.toString());
        System.out.println("");
        System.out.println("Maximum profit is: " + maximumProfit);
    }

    private List<Double> initializeWithZeroes(List<Double> resultVector) {
        for(int i = 0; i < numOfElements; i++){
            resultVector.add(0.0);
        }
        return resultVector;
    }

    private void calculateRatio() {

        for(int i = 0; i < profits.size(); i++){
            ratio.add(profits.get(i)/weights.get(i));
        }
    }
    private void sortByRatio() {
        for (int i = 0; i < numOfElements; i++) {
            for (int j = i + 1; j < numOfElements; j++) {
                if (ratio.get(i) < ratio.get(j)) {
                    swap(i,j);
                }
            }
        }
    }
    private void swap(int i, int j) {
        double temp;
        temp = ratio.get(j);
        ratio.set(j, ratio.get(i));
        ratio.set(i, temp);

        temp = weights.get(j);
        weights.set(j, weights.get(i));
        weights.set(i,  temp);

        temp = profits.get(j);
        profits.set(j, profits.get(i));
        profits.set(i,  temp);

    }

    public static void main(String[] args){
        int numberOfel = 7;
        ArrayList<Double> weightsZ = new ArrayList<Double>(numberOfel);
        ArrayList<Double> profitsZ = new ArrayList<Double>(numberOfel);
        weightsZ.add(2.0);
        weightsZ.add(3.0);
        weightsZ.add(5.0);
        weightsZ.add(7.0);
        weightsZ.add(1.0);
        weightsZ.add(4.0);
        weightsZ.add(1.0);
        profitsZ.add(10.0);
        profitsZ.add(5.0);
        profitsZ.add(15.0);
        profitsZ.add(7.0);
        profitsZ.add(6.0);
        profitsZ.add(18.0);
        profitsZ.add(13.0);

        double capacityZ = 15;

        Knapsack knapsack = new Knapsack(numberOfel, weightsZ, profitsZ,capacityZ);
        knapsack.resolve();

    }

}
