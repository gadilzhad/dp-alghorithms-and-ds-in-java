package com.arshakyan.algorithms.sort;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ezalor on 09.02.2015.
 */
public class Quicksort {
    private List<Integer> body;

    public Quicksort(List<Integer> body) {
        this.body = body;
    }

    public void sort(){
        this.Qsort(0,body.size());
    }

    public void display(){
        System.out.println("----------");
        for(int i = 0; i < body.size(); i++){
            System.out.println( body.get(i));
        }
    }



    private void Qsort(int first, int last){
        if(first >= last){
            return;
        }
        int pivot = partQsort(first, last);
        Qsort(first, pivot-1);
        Qsort(pivot+1, last);
    }



    private int partQsort(int first, int last) {
        int marker = first;
        for(int i = first; i< last; i++){
            if(body.get(i) <= body.get(last-1)){
                swap(i, marker);
                marker++;
            }
        }
        return marker-1;
    }

    private void swap(int i, int marker) {
        Integer temp = body.get(marker); // swap
        body.set(marker, body.get(i));
        body.set(i, temp);
    }


    public static void main(String[] args){
        List test = new ArrayList();
        test.add(3);
        test.add(2);
        test.add(8);
        test.add(124);
        test.add(1);
        Quicksort qsort = new Quicksort(test);
        qsort.display();
        qsort.sort();
        qsort.display();

    }
}
