package com.arshakyan.algorithms.numericalmethods;


/**
 * Created by ezalor on 04.02.2015.
 */
public interface Function {
    public Double compute(Double argument);
}
