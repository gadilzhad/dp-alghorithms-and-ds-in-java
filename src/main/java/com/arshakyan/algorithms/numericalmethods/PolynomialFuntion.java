package com.arshakyan.algorithms.numericalmethods;



import java.util.ArrayList;

/**
 * Created by ezalor on 04.02.2015.
 */
public class PolynomialFuntion implements Function {

    ArrayList<Double> coeffs;
    Integer polynomPower;

    PolynomialFuntion(){
        coeffs = new ArrayList<Double>(5);
        coeffs.add(1.0);
        coeffs.add(2.0);
        coeffs.add(0.0);
        coeffs.add(-4.0);
        coeffs.add(-2.0);
        coeffs.add(2.0);
    }

    PolynomialFuntion(ArrayList<Double> coeffs){
        this.coeffs = coeffs;
    }

    @Override
    public Double compute(Double x) {
        Double result = 0.0;
        for (int exponent = 0; exponent < coeffs.size(); exponent++) {
            result += coeffs.get(exponent)*Math.pow(x,exponent);
        }
        return result;
    }

    public Function diff(){
        ArrayList<Double> newCoeffs = new ArrayList<Double>(coeffs.size()-1);
        for (int exponent = 1; exponent < coeffs.size(); exponent++) {
            newCoeffs.add(coeffs.get(exponent)*exponent);
        }
        return new PolynomialFuntion(newCoeffs) ;
    }
}
