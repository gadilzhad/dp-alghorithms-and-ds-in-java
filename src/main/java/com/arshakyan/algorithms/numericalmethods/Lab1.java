package com.arshakyan.algorithms.numericalmethods;


import com.arshakyan.algorithms.numericalmethods.bisection.BisectionMethod;
import com.arshakyan.algorithms.numericalmethods.linearinterpolation.LinearInterpolationMethod;
import com.arshakyan.algorithms.numericalmethods.newton.NewtonsMethod;

/**
 * Created by ezalor on 04.02.2015.
 */
public class Lab1 {

    public static void main(String[]args){
        double left = -3.0;
        double right = -0.5;
        double eps = 0.00001;
        double x0 = -3.0;


        PolynomialFuntion func = new PolynomialFuntion();
        BisectionMethod bisection = new BisectionMethod(func, left, right, eps);
        NewtonsMethod newtons = new NewtonsMethod(x0, func, eps);
        LinearInterpolationMethod linearInterpolationMethod = new LinearInterpolationMethod(func, left, right, eps);

    }

}
