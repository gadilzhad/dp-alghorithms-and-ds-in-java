package com.arshakyan.algorithms.numericalmethods.newton;


import com.arshakyan.algorithms.numericalmethods.PolynomialFuntion;

/**
 * Created by ezalor on 04.02.2015.
 */
public class NewtonsMethod {
    private double x0;
    private PolynomialFuntion f;
    private double eps;

    public NewtonsMethod(double x0, PolynomialFuntion f, double eps) {
        this.x0 = x0;
        this.f = f;
        this.eps = eps;
        this.snowStartMessage();
        this.compute();
    }

    private void snowStartMessage() {
        System.out.println("Newtons method");

    }


    private double compute() {
            double currentApprox = x0;
            double previousApprox;

            for (; ;) {
                previousApprox = currentApprox;

                this.printCurrentApprox(currentApprox, f.compute(currentApprox));
                currentApprox = newtonStep(currentApprox, f);
                if (Math.abs(f.compute(currentApprox)) < eps || Math.abs(previousApprox - currentApprox) < eps) {

                    return currentApprox;
                }
            }
        }

    private void printCurrentApprox(double currentApprox, double value) {
        System.out.println("Approximation: " + currentApprox + "    Function val: " + value);

    }

    private double newtonStep(double x, PolynomialFuntion f) {
           return x - f.compute(x)/f.diff().compute(x);
        }
    }
