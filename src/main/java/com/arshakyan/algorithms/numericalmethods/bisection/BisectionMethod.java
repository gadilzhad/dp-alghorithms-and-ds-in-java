package com.arshakyan.algorithms.numericalmethods.bisection;


import com.arshakyan.algorithms.numericalmethods.Function;

/**
 * Created by ezalor on 04.02.2015.
 */

public class BisectionMethod
{
    double a;
    double b;
    Function function;
    private double eps;


    public BisectionMethod(Function function, double a, double b, double stoppingErrorApprox)
    {
        this.a = a;
        this.b = b;
        this.function = function;
        this.eps = stoppingErrorApprox;
        this.execute();
    }

    private double getMidPoint(double a, double b)
    {
        return (b - a)/2;
    }

    private boolean sameSign(double a, double b)
    {
        if(function.compute(a) * function.compute(b) > 0)
            return true;
        else{
        return false;
        }
    }
    private void reassigment(double c)
    {
        if(sameSign(a, c))
        {
            a = c;
        }
        else
        {
            b = c;
        }
    }


    protected void execute()
    {
        printStartMessage();
        if(sameSign(a,b))
        {
            System.out.println("Functions of a & b have the same sign!");
            System.out.println("Exiting.\n");
            return;
        }

        double c = getMidPoint(a, b) + a;
        double currentApproximate = c;


        reassigment(c);
        for(;;)
        {
            c = getMidPoint(a, b) + a;
            currentApproximate = function.compute(c);
            if(calculateError(a,b))
            {
                System.out.println("Convergence! Estimated root is: " + c + "\n");
                return;
            }
            printCurrentBordersWithGuessedRoot(c);
            reassigment(c);
        }}

    private void printCurrentBordersWithGuessedRoot(double c) {
        System.out.println("a : : " + a + " | b : " + b + "     guessed root: " + c );
    }

    private boolean calculateError(double a, double b) {
        if(Math.abs(a - b) < eps){
            return true;
        }
        return false;
    }

    protected void printStartMessage()
    {
            System.out.println("Bisection method");

    }
}