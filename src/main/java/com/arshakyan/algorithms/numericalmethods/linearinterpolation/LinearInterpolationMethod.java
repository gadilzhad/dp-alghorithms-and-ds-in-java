package com.arshakyan.algorithms.numericalmethods.linearinterpolation;


import com.arshakyan.algorithms.numericalmethods.Function;

/**
 * Created by ezalor on 05.02.2015.
 */
public class LinearInterpolationMethod {
        double a;
        double b;
        Function function;
        private double eps;


        public LinearInterpolationMethod(Function function, double a, double b, double stoppingErrorApprox)
        {
            this.a = a;
            this.b = b;
            this.function = function;
            this.eps = stoppingErrorApprox;
            this.execute();
        }

        private double getMidPoint(double a, double b)
        {
            return (a*function.compute(b) - b*function.compute(a))/(function.compute(b)-function.compute(a));
        }

        private boolean sameSign(double a, double b)
        {
            if(function.compute(a) * function.compute(b) > 0)
                return true;
            else{
                return false;
            }
        }
        private void reassigment(double c)
        {
            if(sameSign(a, c))
            {
                a = c;
            }
            else
            {
                b = c;
            }
        }


        protected void execute()
        {
            printStartMessage();
            if(sameSign(a,b))
            {
                System.out.println("Functions of a & b have the same sign!");
                System.out.println("Exiting.\n");
                return;
            }

            double c = this.getMidPoint(a,b);
            double currentApproximate = c;

            double previousC;

            reassigment(c);
            for(int i = 0; true; i++)
            {
                previousC = c;
                c = getMidPoint(a, b);
                currentApproximate = function.compute(c);
                if(calculateError(c, previousC))
                {
                    System.out.println("Convergence! Estimated root is: " + c + "\n");
                    return;
                }
                if(i % 25 == 0) {
                    printCurrentBordersWithGuessedRoot(c);
                }
                reassigment(c);
            }}

        private void printCurrentBordersWithGuessedRoot(double c) {
            System.out.println("a : : " + a + " | b : " + b + "     guessed root: " + c );
        }

        private boolean calculateError(double c, double prev) {
            if(Math.abs(function.compute(c)) < eps){
                return true;
            }
            if(Math.abs(c-prev) < eps){
                return true;
            }
            return false;
        }

        protected void printStartMessage()
        {
            System.out.println("Liner Interpolation method");

        }
    }

